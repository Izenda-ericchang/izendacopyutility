﻿using System.ComponentModel;
using System.Windows.Input;

namespace CopyManagementUtility.ViewModel
{
    public interface ICopyManagementVM : INotifyPropertyChanged
    {
        #region Properties
        string AccessToken { get; set; }

        string BaseUrl { get; set; }

        bool SelectAllTenants { get; set; }

        string CopySchemaSource { get; set; }

        string SchemaList { get; set; }

        string CopySchemaTenantList { get; set; }

        string CopyRoleSource { get; set; }

        string RoleList { get; set; }

        string CopyRoleTenantList { get; set; }

        bool ConfigurationConfirmed { get; set; }

        bool IsProcessing { get; }

        bool IsCopySchemaProcessing { get; set; }

        bool IsCopyRoleProcessing { get; set; }

        ICommand ValidateParameterCommand { get; }

        ICommand CopySchemaCommand { get; }

        ICommand CopyRoleCommand { get; }

        ICommand ResetFieldCommand { get; }
        #endregion
    }
}
