﻿using CopyManagementUtility.IzendaBoundary.IzendaApiService;
using CopyManagementUtility.IzendaBoundary.IzendaModel;
using CopyManagementUtility.Utilities;
using Izenda.BI.Framework.Models;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;

namespace CopyManagementUtility.ViewModel
{
    public class CopyManagementVM : ICopyManagementVM
    {
        #region Variables
        private string _accessToken = string.Empty;
        private string _baseUrl = string.Empty;

        private string _copySchemaSource = string.Empty;
        private string _schemaList = string.Empty;
        private string _copySchemaTenantList = string.Empty;

        private string _copyRoleSource = string.Empty;
        private string _roleList = string.Empty;
        private string _copyRoleTenantList = string.Empty;

        private ICommand _validateParameterCommand;
        private ICommand _copySchemaCommand;
        private ICommand _copyRoleCommand;
        private ICommand _resetFieldCommand;

        private bool _configurationConfirmed = false;
        private bool _isCopySchemaProcessing = false;
        private bool _isCopyRoleProcessing = false;
        private bool _selectAllTenants = false;

        private ILog _log = LogManager.GetLogger(typeof(App));

        private string _hostname = string.Empty;
        private BackgroundWorker _backGroundWorker;
        #endregion

        #region Properties
        /// <summary>
        /// Access token for connection.
        /// Can grab this value from dev tool.
        /// </summary>
        public string AccessToken
        {
            get => _accessToken;
            set
            {
                if (_accessToken != value)
                {
                    _accessToken = value;

                    ConfigurationConfirmed = false;
                    OnPropertyChanged(nameof(AccessToken));
                }
            }
        }

        /// <summary>
        /// Base url for connection.
        /// e.g. http://localhost:81/api/"
        /// </summary>
        public string BaseUrl
        {
            get => _baseUrl;
            set
            {
                if (_baseUrl != value)
                {
                    _baseUrl = value;

                    ConfigurationConfirmed = false;
                    OnPropertyChanged(nameof(BaseUrl));
                }
            }
        }

        /// <summary>
        /// Boolean flag whether user select all tenants.
        /// </summary>
        public bool SelectAllTenants
        {
            get => _selectAllTenants;
            set
            {
                if (_selectAllTenants != value)
                {
                    _selectAllTenants = value;

                    if (_selectAllTenants)
                    {
                        CopyRoleTenantList = string.Empty;
                        CopySchemaTenantList = string.Empty;
                    }

                    OnPropertyChanged(nameof(SelectAllTenants));
                }
            }
        }

        /// <summary>
        /// Used for UI. Boolean flag for confirming user input for connection is valid (access token and base url).
        /// UI will be enabled for input field on copy functions.
        /// </summary>
        public bool ConfigurationConfirmed
        {
            get => _configurationConfirmed;
            set
            {
                if (_configurationConfirmed != value)
                {
                    _configurationConfirmed = value;
                    OnPropertyChanged(nameof(ConfigurationConfirmed));
                }
            }
        }

        /// <summary>
        /// Source for copy.
        /// If this is empty, source will be automatically system level one.
        /// </summary>
        public string CopySchemaSource
        {
            get => _copySchemaSource;
            set
            {
                if (_copySchemaSource != value)
                {
                    _copySchemaSource = value;
                    OnPropertyChanged(nameof(CopySchemaSource));
                }
            }
        }

        /// <summary>
        /// User input for schema list. 
        /// Delimeter is comma.
        /// </summary>
        public string SchemaList
        {
            get => _schemaList;
            set
            {
                if (_schemaList != value)
                {
                    _schemaList = value;
                    OnPropertyChanged(nameof(SchemaList));
                }
            }
        }

        /// <summary>
        /// User input for copy schema tenant list (target)
        /// Delimeter is comma.
        /// If SelectAllTenants is true, then this list will be cleared. 
        /// Target will be automatically all tenants in server.
        /// </summary>
        public string CopySchemaTenantList
        {
            get => _copySchemaTenantList;
            set
            {
                if (_copySchemaTenantList != value)
                {
                    _copySchemaTenantList = value;
                    OnPropertyChanged(nameof(CopySchemaTenantList));
                }
            }
        }

        /// <summary>
        /// Source for copy.
        /// If this is empty, source will be automatically system level one.
        /// </summary>
        public string CopyRoleSource
        {
            get => _copyRoleSource;
            set
            {
                if (_copyRoleSource != value)
                {
                    _copyRoleSource = value;
                    OnPropertyChanged(nameof(CopyRoleSource));
                }
            }
        }

        /// <summary>
        /// User input for role list. 
        /// Delimeter is comma.
        /// </summary>
        public string RoleList
        {
            get => _roleList;
            set
            {
                if (_roleList != value)
                {
                    _roleList = value;
                    OnPropertyChanged(nameof(RoleList));
                }
            }
        }

        /// <summary>
        /// User input for copy role tenant list (target)
        /// Delimeter is comma.
        /// If SelectAllTenants is true, then this list will be cleared. 
        /// Target will be automatically all tenants in server.
        /// </summary>
        public string CopyRoleTenantList
        {
            get => _copyRoleTenantList;
            set
            {
                if (_copyRoleTenantList != value)
                {
                    _copyRoleTenantList = value;
                    OnPropertyChanged(nameof(CopyRoleTenantList));
                }
            }
        }

        /// <summary>
        /// Boolean property to check whether Copy Schema or Copy Role is processing.
        /// If processing, then disable some UIs to prevent unexpected errors
        /// </summary>
        public bool IsProcessing => IsCopySchemaProcessing || IsCopyRoleProcessing;

        /// <summary>
        /// Boolean property to check whether Copy Schema is processing.
        /// If processing, then disable some UIs to prevent unexpected errors
        /// </summary>
        public bool IsCopySchemaProcessing
        {
            get => _isCopySchemaProcessing;
            set
            {
                if (_isCopySchemaProcessing != value)
                {
                    _isCopySchemaProcessing = value;
                    OnPropertyChanged(nameof(IsCopySchemaProcessing));
                }
            }
        }

        /// <summary>
        /// Boolean property to check whether Copy Role is processing.
        /// If processing, then disable some UIs to prevent unexpected errors
        /// </summary>
        public bool IsCopyRoleProcessing
        {
            get => _isCopyRoleProcessing;
            set
            {
                if (_isCopyRoleProcessing != value)
                {
                    _isCopyRoleProcessing = value;
                    OnPropertyChanged(nameof(IsCopyRoleProcessing));
                }
            }
        }

        /// <summary>
        /// Validate user input for establishing connection
        /// </summary>
        public ICommand ValidateParameterCommand
        {
            get
            {
                if (_validateParameterCommand == null)
                    _validateParameterCommand = new RelayCommand(c => !string.IsNullOrEmpty(AccessToken) && !string.IsNullOrEmpty(BaseUrl), c => OnValidateParameterCommand());

                return _validateParameterCommand;
            }
        }

        /// <summary>
        /// Copy Schema ICommand.
        /// </summary>
        public ICommand CopySchemaCommand
        {
            get
            {
                if (_copySchemaCommand == null)
                    _copySchemaCommand = new RelayCommand(c => CanCopySchema(), c => OnCopySchemaCommand());

                return _copySchemaCommand;
            }
        }

        /// <summary>
        /// Copy Role ICommand.
        /// </summary>
        public ICommand CopyRoleCommand
        {
            get
            {
                if (_copyRoleCommand == null)
                    _copyRoleCommand = new RelayCommand(c => CanCopyRole(), c => OnCopyRoleCommand());

                return _copyRoleCommand;
            }
        }

        /// <summary>
        /// Reset Field ICommand. Clear all fields.
        /// </summary>
        public ICommand ResetFieldCommand
        {
            get
            {
                if (_resetFieldCommand == null)
                    _resetFieldCommand = new RelayCommand(c => CanCopyRole() || CanCopySchema(), c => OnResetFieldCommnad());

                return _resetFieldCommand;
            }
        }
        #endregion

        #region CTOR
        public CopyManagementVM()
        {
            _backGroundWorker = new BackgroundWorker();
            _backGroundWorker.DoWork += new DoWorkEventHandler(BackgrounWorker_DoWork);
            _backGroundWorker.ProgressChanged += new ProgressChangedEventHandler(BackgroundWorker_ProgressChanged);
            _backGroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(BackgroundWorker_RunWorkerCompleted);
        }
        #endregion

        #region Event Handlers
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Methods
        protected void OnPropertyChanged([CallerMemberName] string name = null) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));

        /// <summary>
        /// Validate the current access token with base url.
        /// </summary>
        private void OnValidateParameterCommand()
        {
            ConfigurationConfirmed = true;

            if (Uri.TryCreate(BaseUrl.Trim(), UriKind.Absolute, out Uri outUri) && (outUri.Scheme == Uri.UriSchemeHttp || outUri.Scheme == Uri.UriSchemeHttps))
                _hostname = outUri.ToString().Replace(outUri.PathAndQuery, "");

            // Test connection to verify access token is valid
            bool isValidConnection = true;

            try
            {
                // test endpoint get all tenants with deserializing process
                var response = IzendaService.IzendaApi_Request(_hostname + IzEndPointString.GET_ALLTENANTS, AccessToken);
                var tenantListFromServer = JsonConvert.DeserializeObject<List<Tenants>>(response);
            }
            catch (Exception ex)
            {
                _log.Info("Invalid connection: " + ex.Message);
                isValidConnection = false;
            }

            if (string.IsNullOrEmpty(AccessToken) || outUri == null || !isValidConnection)
            {
                MessageBox.Show("Please provide access token and base url correctly.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                ConfigurationConfirmed = false;
            }
            else
            {
                _log.Info("Valid connection established with current base url and access token.");
                ConfigurationConfirmed = true;
            }
        }

        /// <summary>
        /// Invoke background worker to conduct Copy Schema
        /// </summary>
        private void OnCopySchemaCommand() => _backGroundWorker.RunWorkerAsync(IzendaCopyMode.CopyScheme);

        /// <summary>
        /// Invoke background worker to conduct Copy Role(s)
        /// </summary>
        private void OnCopyRoleCommand() => _backGroundWorker.RunWorkerAsync(IzendaCopyMode.CopyRole);

        /// <summary>
        /// Conduct background work according to user selection (e.g., Copy Scheme, Copy Role..)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BackgrounWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            var stopWatch = Stopwatch.StartNew();
            _log.Info("TimeStamp Start " + DateTime.Now.ToString());

            switch ((IzendaCopyMode)e.Argument)
            {
                case IzendaCopyMode.CopyScheme:
                    {
                        IsCopySchemaProcessing = true;
                        OnPropertyChanged(nameof(IsProcessing));

                        CopySchema();

                        break;
                    }

                case IzendaCopyMode.CopyRole:
                    {
                        IsCopyRoleProcessing = true;
                        OnPropertyChanged(nameof(IsProcessing));

                        CopyRole();

                        break;
                    }

                default: // not expected
                    break;
            }

            _log.Info("TimeStamp End " + DateTime.Now.ToString());
            stopWatch.Stop();

            _log.Info("Total Time Taken: " + stopWatch.Elapsed);
            Console.WriteLine("\nTotal Time Taken " + stopWatch.Elapsed);
        }

        /// <summary>
        /// Copy work completed. Notify spinner to stop.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            IsCopySchemaProcessing = false;
            IsCopyRoleProcessing = false;

            OnPropertyChanged(nameof(IsProcessing));
        }

        private void BackgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            // leave this block as blank. Use if required
        }

        /// <summary>
        /// Copy Schema
        /// </summary>
        private void CopySchema()
        {
            var tenantList = !SelectAllTenants ? CopySchemaTenantList.Split(',').ToList() : GetAllTenants();
            var connList = SchemaList.Split(',').ToList();

            if (tenantList.Any())
            {
                var result = IzendaBoundary.CopySchema.ProcessCopySchema(_hostname, AccessToken, tenantList, connList, CopySchemaSource);

                if (result)
                    MessageBox.Show("Copy Schema Completed", "Completed", MessageBoxButton.OK, MessageBoxImage.Information);
                else
                    MessageBox.Show("Copy Schema Error. Please check your log", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                _log.Info("There is no tenant in database. Stop processing!");
                MessageBox.Show("There is no tenant in your system. Please add tenant(s) to proceed.", "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        /// <summary>
        /// Copy Role(s)
        /// </summary>
        private void CopyRole()
        {
            var tenantList = !SelectAllTenants ? CopyRoleTenantList.Split(',').ToList() : GetAllTenants();
            var roleList = RoleList.Split(',').ToList();

            if (tenantList.Any())
            {
                var result = IzendaBoundary.CopyRole.ProcessCopyRole(_hostname, AccessToken, tenantList, roleList, CopyRoleSource);

                if (result)
                    MessageBox.Show("Copy Role Completed", "Completed", MessageBoxButton.OK, MessageBoxImage.Information);
                else
                    MessageBox.Show("Copy Role Error. Please check your log", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                _log.Info("There is no tenant in database. Stop processing!");
                MessageBox.Show("There is no tenant in your system. Please add tenant(s) to proceed.", "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        /// <summary>
        /// Retrieve all tenants from database
        /// </summary>
        /// <returns>tenant list</returns>
        private IEnumerable<string> GetAllTenants()
        {
            var response = IzendaService.IzendaApi_Request(_hostname + IzEndPointString.GET_ALLTENANTS, AccessToken);
            var tenantListFromServer = JsonConvert.DeserializeObject<List<Tenants>>(response);

            var allTenants = new List<string>();

            foreach (var tenant in tenantListFromServer)
            {
                allTenants.Add(tenant.TenantID);
            }

            return allTenants;
        }

        /// <summary>
        /// CanExecute condition for CopySchema command.
        /// </summary>
        /// <returns>bool</returns>
        private bool CanCopySchema() => SelectAllTenants
                ? !string.IsNullOrEmpty(SchemaList)
                : !string.IsNullOrEmpty(SchemaList) && !string.IsNullOrEmpty(CopySchemaTenantList);

        /// <summary>
        /// CanExecute condition for CopyRole command.
        /// </summary>
        /// <returns>bool</returns>
        private bool CanCopyRole() => SelectAllTenants
                ? !string.IsNullOrEmpty(RoleList)
                : !string.IsNullOrEmpty(RoleList) && !string.IsNullOrEmpty(CopyRoleTenantList);

        /// <summary>
        /// Clear out all fields
        /// </summary>
        private void OnResetFieldCommnad()
        {
            CopySchemaSource = string.Empty;
            SchemaList = string.Empty;
            CopySchemaTenantList = string.Empty;

            CopyRoleSource = string.Empty;
            RoleList = string.Empty;
            CopyRoleTenantList = string.Empty;
        }
        #endregion
    }
}
