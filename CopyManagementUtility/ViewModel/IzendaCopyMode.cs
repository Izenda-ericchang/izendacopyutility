﻿namespace CopyManagementUtility.ViewModel
{
    /// <summary>
    /// Enum value to define which copy function is currently selected.
    /// </summary>
    public enum IzendaCopyMode
    {
        CopyScheme,
        CopyRole
    }
}
