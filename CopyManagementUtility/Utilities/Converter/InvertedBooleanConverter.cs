﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace CopyManagementUtility.Utilities.Converter
{
    /// <summary>
    /// inverted boolean value converter for xaml
    /// </summary>
    public class InvertedBooleanConverter : IValueConverter
    {
        #region Methods
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (targetType != typeof(bool))
                throw new InvalidOperationException("The target must be a boolean value!");

            return !(bool)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => throw new NotSupportedException();
        #endregion
    }
}
