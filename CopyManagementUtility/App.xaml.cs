﻿using log4net;
using log4net.Repository.Hierarchy;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Xml;

namespace CopyManagementUtility
{
    public partial class App : Application
    {
        #region Variables
        private ILog _log = LogManager.GetLogger(typeof(App)); 
        #endregion

        #region CTOR
        public App()
        {
            var log4netConfig = new XmlDocument();
            log4netConfig.Load(File.OpenRead("log4net.config"));
            var repo = LogManager.CreateRepository(Assembly.GetEntryAssembly(), typeof(Hierarchy));
            log4net.Config.XmlConfigurator.Configure(repo, log4netConfig["log4net"]);

            _log.Info("Copy Management Application Started");
        } 
        #endregion
    }
}
