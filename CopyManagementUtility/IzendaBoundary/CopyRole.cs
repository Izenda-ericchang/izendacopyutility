﻿using CopyManagementUtility.IzendaBoundary.IzendaApiService;
using CopyManagementUtility.IzendaBoundary.IzendaModel;
using Izenda.BI.Framework.Models;
using Izenda.BI.Framework.Models.Permissions;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CopyManagementUtility.IzendaBoundary
{
    public class CopyRole
    {
        #region Variables
        private static ILog _log = LogManager.GetLogger(typeof(App));
        private static string _hostname = string.Empty;
        private static List<string> _tempMatchList1 = new List<string>();
        private static List<string> _tempMatchList2 = new List<string>();
        private static bool _completed = false;
        #endregion

        #region Methods
        /// <summary>
        /// Receive all params and set local variables
        /// </summary>
        /// <param name="hostname"></param>
        /// <param name="token"></param>
        /// <param name="tenantList"></param>
        /// <param name="roleList"></param>
        /// <param name="copySource"></param>
        /// <returns></returns>
        public static bool ProcessCopyRole(string hostname, string token, IEnumerable<string> tenantList, IEnumerable<string> roleList, string copySource)
        {
            ResetTempLists();
            
            _hostname = hostname;
            _completed = true;

            PreprareRoleCopy(token, tenantList, roleList, copySource);

            return _completed;
        }

        /// <summary>
        /// Pre-process before update tenant role
        /// </summary>
        /// <param name="token"></param>
        /// <param name="tenantList"></param>
        /// <param name="roleList"></param>
        /// <param name="copySource"></param>
        private static void PreprareRoleCopy(string token, IEnumerable<string> tenantList, IEnumerable<string> roleList, string copySource)
        {
            try
            {
                var response = IzendaService.IzendaApi_Request(_hostname + IzEndPointString.GET_ALLTENANTS, token);
                var tenantListFromServer = JsonConvert.DeserializeObject<List<Tenants>>(response);
                var source = tenantListFromServer.FirstOrDefault(t => t.TenantID == copySource);

                UpdateTenantRole(token, tenantList, roleList, source?.Id);
            }
            catch (Exception ex)
            {
                _completed = false;

                Console.WriteLine("Can't process your request. Error:  " + ex.Message);
                _log.Info("Can't process your request. Error:  " + ex.Message);
            }
        }

        /// <summary>
        /// Update Role for Tenant
        /// </summary>
        /// <param name="token"></param>
        /// <param name="tenantList"></param>
        /// <param name="roleList"></param>
        /// <param name="tenantId"></param>
        private static void UpdateTenantRole(string token, IEnumerable<string> tenantList, IEnumerable<string> roleList, Guid? tenantId)
        {
            var matchedTenantList = MatchTenant(tenantList, token);
            var matchedRoleList = MatchRole(roleList, token, tenantId);

            // TODO: Improve this -> use multi-thread approach.
            foreach (var tenant in matchedTenantList)
            {
                // Fetch the Tenant Connection detail
                var jsonTenantConnDetail = IzendaService.IzendaApi_Request(_hostname + IzEndPointString.CONNECTION + "/" + tenant.Id.ToString(), token);
                dynamic dataConnTenant = JArray.Parse(jsonTenantConnDetail);

                if (dataConnTenant.Count > 0)
                {
                    Console.WriteLine("\nTenant has total connection: " + dataConnTenant.Count + " , Tenant: " + tenant.Name);
                    _log.Info("Tenant has total connection: " + dataConnTenant.Count + " , Tenant: " + tenant.Name);
                }
                else
                {
                    Console.WriteLine("Tenant has No Data Source!, Tenant: " + tenant.Name);
                    _log.Info("Tenant has No Data Source!, Tenant: " + tenant.Name);

                    continue;
                }

                //Fetch Tenant Role
                var jsonAllTenantRoles = IzendaService.IzendaApi_Request(_hostname + IzEndPointString.GET_ALL_ROLES + tenant.Id, token);
                dynamic dataTenantRoles = JArray.Parse(jsonAllTenantRoles);
                List<IzUserRoleDetail> allUserRoleDetail = JsonConvert.DeserializeObject<List<IzUserRoleDetail>>(Convert.ToString(dataTenantRoles));

                if (dataTenantRoles.Count > 0)
                {
                    Console.WriteLine("Total Tenant Roles are: " + dataTenantRoles.Count + " Tenant:" + tenant.Name);
                    _log.Info("Total Tenant Roles are: " + dataTenantRoles.Count + " Tenant:" + tenant.Name);
                }

                foreach (var role in matchedRoleList)
                {
                    if (dataTenantRoles.Count > 0)
                    {
                        var findTenantRole = allUserRoleDetail.FirstOrDefault(m => m.Name == role.Name);
                        if (findTenantRole == null) // if not found... create
                        {
                            try
                            {
                                CreateRole(role, tenant, token, dataConnTenant, tenantId);
                            }
                            catch (Exception ex)
                            {
                                _completed = false;

                                Console.WriteLine("While Creating, Role:" + role.Name + " for Tenant:" + tenant.Name + " has error while processing. Error:  " + ex.Message);
                                _log.Info("While Creating, Role:" + role.Name + " for Tenant:" + tenant.Name + " has error while processing. Error:  " + ex.Message);
                            }
                        }
                        else // if found...update
                        {
                            try
                            {
                                UpdateRole(role, tenant, token, dataConnTenant, findTenantRole.Id.ToString(), tenantId);
                            }
                            catch (Exception ex)
                            {
                                _completed = false;

                                Console.WriteLine("While Updating, Role:" + role.Name + " for Tenant:" + tenant.Name + " has error while processing. Error:  " + ex.Message);
                                _log.Info("While Updating, Role:" + role.Name + " for Tenant:" + tenant.Name + " has error while processing. Error:  " + ex.Message);
                            }
                        }
                    }
                    else
                    {
                        // need to Create all role to Tenant, if Tenant doesn't have any role
                        try
                        {
                            CreateRole(role, tenant, token, dataConnTenant, tenantId);
                        }
                        catch (Exception ex)
                        {
                            _completed = false;

                            Console.WriteLine("While Creating, Role:" + role.Name + " for Tenant:" + tenant.Name + " has error while processing. Error:  " + ex.Message);
                            _log.Info("While Creating, Role:" + role.Name + " for Tenant:" + tenant.Name + " has error while processing. Error:  " + ex.Message);
                        }
                    }

                    ResetTempLists();
                }
            }
        }

        /// <summary>
        /// Fetch and Match Tenant
        /// </summary>
        /// <param name="currentTenantList"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        private static List<IzTenant> MatchTenant(IEnumerable<string> currentTenantList, string token)
        {
            var response = IzendaService.IzendaApi_Request(_hostname + IzEndPointString.GET_ALLTENANTS, token);
            var allTenantsFromServer = JsonConvert.DeserializeObject<List<Tenants>>(response);
            var tenantList = new List<IzTenant>();

            foreach (var currentTenant in currentTenantList)
            {
                foreach (var tenantFromServer in allTenantsFromServer)
                {
                    //match tenant
                    if (Convert.ToString(tenantFromServer.TenantID).ToUpper().Trim() == currentTenant.ToUpper().Trim())
                    {
                        //Get all matched tenant list
                        var izTenant = new IzTenant
                        {
                            Id = tenantFromServer.Id,
                            Name = tenantFromServer.Name
                        };

                        tenantList.Add(izTenant);
                    }
                }
            }

            return tenantList;
        }

        /// <summary>
        /// Fetch and Match Role
        /// </summary>
        /// <param name="currentRoleList"></param>
        /// <param name="token"></param>
        /// <param name="tenantId"></param>
        /// <returns></returns>
        private static List<IzRole> MatchRole(IEnumerable<string> currentRoleList, string token, Guid? tenantId)
        {
            var matchedRoles = new List<IzRole>();
            dynamic sourceRoles = null;

            var response = tenantId == null ? 
                IzendaService.IzendaApi_Request(_hostname + IzEndPointString.GET_ALL_ROLES_OPTIONS, token) 
                : IzendaService.IzendaApi_Request(_hostname + IzEndPointString.GET_ALL_ROLES + tenantId, token);

            sourceRoles = JArray.Parse(response);

            if (sourceRoles.Count > 0)
            {
                Console.WriteLine("\nTotal Source Roles are: " + sourceRoles.Count);
                _log.Info("Total Source Roles are: " + sourceRoles.Count);

                foreach (var sourceRole in sourceRoles)
                {
                    foreach (var currentRole in currentRoleList)
                    {
                        //match Role
                        if (Convert.ToString(sourceRole.name).ToUpper().Trim() == currentRole.ToUpper().Trim())
                        {
                            var izRole = new IzRole
                            {
                                RoleId = sourceRole.id,
                                Name = sourceRole.name
                            };

                            matchedRoles.Add(izRole);
                        }
                    }
                }
            }
            else
            {
                _completed = false;

                Console.WriteLine("\nThere is no role to be copied. Process canceled.");
                _log.Info("There is no role to be copied. Process canceled.");
            }

            return matchedRoles;
        }

        /// <summary>
        /// Create New Role for Tenant
        /// </summary>
        /// <param name="izRole"></param>
        /// <param name="izTenant"></param>
        /// <param name="token"></param>
        /// <param name="dataConnTenant"></param>
        /// <param name="tenantId"></param>
        private static void CreateRole(IzRole izRole, IzTenant izTenant, string token, dynamic dataConnTenant, Guid? tenantId)
        {
            bool iiCheck = false;

            // Fetch Each Role by Roleid
            var jsonRole = IzendaService.IzendaApi_Request(_hostname + IzEndPointString.ROLE + izRole.RoleId.ToString(), token);

            if (!string.IsNullOrEmpty(jsonRole))
            {
                // Permissions
                dynamic dRole = JObject.Parse(jsonRole);
                var allpermissionDetail = JsonConvert.DeserializeObject<Permission>(Convert.ToString(dRole.permission));

                // Fetch DataModel by Roleid
                string prefix = "{'tenantId':null,'roleId':'";
                if (tenantId != null)
                    prefix = "{'tenantId':'" + tenantId.ToString() + "','roleId':'";

                var strPostDataModel = prefix + izRole.RoleId.ToString() + "','skipItems':0,'pageSize':100000,'parentIds':[],'visibleQuerySourcesTree':[],'criteria':[{'key':'DataSourceName','value':''},{'key':'ShowCheckedQuerySource','value':false}]}";
                var jsonPartialDataModel = IzendaService.IzendaApi_RequestBody(_hostname + IzEndPointString.LOAD_PARTIAL_DATAMODEL_ACCESS, token, strPostDataModel);

                if (!string.IsNullOrEmpty(jsonPartialDataModel))
                {
                    // visibleQuerySourcesTree
                    dynamic dDataModel = JObject.Parse(jsonPartialDataModel);
                    var visibleQuerySources = JsonConvert.DeserializeObject<List<DataSourceItem>>(Convert.ToString(dDataModel.data));
                    var savedRoleid = Guid.Empty;

                    foreach (var connTenant in dataConnTenant)
                    {
                        foreach (var querySource in visibleQuerySources)
                        {
                            var conn = querySource.Name.Substring(querySource.Name.IndexOf(']') + 1).Trim().ToUpper();
                            if (conn == Convert.ToString(connTenant.name).Trim().ToUpper())
                            {
                                //Temp stored for validation, connection and tenant detail
                                iiCheck = true;

                                var match = _tempMatchList1.FirstOrDefault(tt => tt.Contains(Convert.ToString(izTenant.Id.ToString() + querySource.Name).ToUpper()));
                                if (match != null)
                                    continue;

                                Console.WriteLine("Source Role with Tenant is matched. Process Started for Role Creation, " + querySource.Name + " Tenant:" + izTenant.Name);
                                _log.Info("Source Role with Tenant is matched.  Process Started for Role Creation, " + querySource.Name + " Tenant:" + izTenant.Name);

                                List<DataSourceItem> newChild = null;

                                //Fetch Tenant loadPartialDataModelAccess
                                var strDataModelTenant = "{'tenantId':'" + izTenant.Id.ToString() + "','roleId':null,'skipItems':0,'pageSize':100000,'parentIds':[],'visibleQuerySourcesTree':[],'criteria':[{'key':'DataSourceName','value':''},{'key':'ShowCheckedQuerySource','value':false}]}";
                                var jsonPartialDataModelTenant = IzendaService.IzendaApi_RequestBody(_hostname + IzEndPointString.LOAD_PARTIAL_DATAMODEL_ACCESS, token, strDataModelTenant);

                                if (!string.IsNullOrEmpty(jsonPartialDataModelTenant))
                                {
                                    dynamic dDataModelTenant = JObject.Parse(jsonPartialDataModelTenant);
                                    var oTenantDataModel = JsonConvert.DeserializeObject<List<DataSourceItem>>(Convert.ToString(dDataModelTenant.data));

                                    foreach (var conCheck in oTenantDataModel)
                                    {
                                        if (Convert.ToString(querySource.Name).Trim().ToUpper() == Convert.ToString(conCheck.Name).Trim().ToUpper())
                                        {
                                            //For Vaidation maintain a List
                                            _tempMatchList1.Add(Convert.ToString(izTenant.Id.ToString() + querySource.Name).ToUpper());

                                            var oTenantDataModel1 = new List<DataSourceItem>();
                                            oTenantDataModel1.Add(conCheck);

                                            var oSourceDataModel = new List<DataSourceItem>();
                                            oSourceDataModel.Add(querySource);

                                            newChild = CheckInDataSourceItems(oSourceDataModel, oTenantDataModel1);

                                            //User + Permissions + visibleQuerySourcesTree
                                            var izRoleDetail = new IzRoleDetail()
                                            {
                                                Users = null,
                                                Permission = allpermissionDetail,
                                                VisibleQuerySources = null,
                                                Name = izRole.Name,
                                                TenantId = izTenant.Id.ToString(),
                                                Active = true,
                                                IsDirty = true, //false,

                                                Deleted = false,
                                                State = 3,  //0,
                                                CreatedBy = "Automation Script",
                                                ModifiedBy = "Automation Script",

                                                Id = (savedRoleid == null) ? null : savedRoleid.ToString(),     //for new creating role
                                                VisibleQuerySourcesTree = newChild, //visibleQuerySources, //lstDataSourceItem, //
                                                ShowCheckedQuerySource = false,

                                                Inserted = true,
                                                Version = 2,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now
                                            };

                                            var jsonRoleSchema = JsonConvert.SerializeObject(izRoleDetail);
                                            var sResultRole = IzendaService.IzendaApi_RequestBody(_hostname + IzEndPointString.ROLE, token, jsonRoleSchema);
                                            dynamic dataRoleResult = JObject.Parse(sResultRole);

                                            if (dataRoleResult.success.ToObject<bool>())
                                            {
                                                savedRoleid = dataRoleResult.role.id;

                                                _log.Info("Output: Success, Role " + izRole.Name + " " + dataRoleResult.messages + " Tenant: " + izTenant.Name);
                                                Console.WriteLine("Output: Success, Role " + izRole.Name + " " + dataRoleResult.messages + " Tenant: " + izTenant.Name);
                                            }
                                            else
                                            {
                                                _log.Info("Output: Failure " + dataRoleResult.messages + " Tenant: " + izTenant.Name);
                                                Console.WriteLine("Output: Failure " + dataRoleResult.messages + " Tenant: " + izTenant.Name);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (iiCheck)
            {
                _log.Info("Detail not Found, Tenant:" + izTenant.Name.ToString() + " Role:" + izRole.Name);
                Console.WriteLine("Detail not Found, Tenant:" + izTenant.Name.ToString() + " Role:" + izRole.Name);
            }
        }

        /// <summary>
        /// Update Role for Tenant
        /// </summary>
        /// <param name="sysRole"></param>
        /// <param name="izTenant"></param>
        /// <param name="token"></param>
        /// <param name="dataConnTenant"></param>
        /// <param name="tenantRoleId"></param>
        /// <param name="tenantId"></param>
        private static void UpdateRole(IzRole sysRole, IzTenant izTenant, string token, dynamic dataConnTenant, string tenantRoleId, Guid? tenantId)
        {
            //Fetch Each Role by Roleid
            var jsonRole = IzendaService.IzendaApi_Request(_hostname + IzEndPointString.ROLE + sysRole.RoleId.ToString(), token);

            if (!string.IsNullOrEmpty(jsonRole))
            {
                // 1 Permissions
                dynamic dRole = JObject.Parse(jsonRole);
                var allpermissionDetail = JsonConvert.DeserializeObject<Permission>(Convert.ToString(dRole.permission));

                // 2 Fetch DataModel by Roleid
                string prefix = "{'tenantId':null,'roleId':'";
                if (tenantId != null)
                    prefix = "{'tenantId':'" + tenantId.ToString()+"','roleId':'";

                var strPostDataModel = prefix + sysRole.RoleId.ToString() + "','skipItems':0,'pageSize':100000,'parentIds':[],'visibleQuerySourcesTree':[],'criteria':[{'key':'DataSourceName','value':''},{'key':'ShowCheckedQuerySource','value':false}]}";
                var jsonPartialDataModel = IzendaService.IzendaApi_RequestBody(_hostname + IzEndPointString.LOAD_PARTIAL_DATAMODEL_ACCESS, token, strPostDataModel);

                if (!string.IsNullOrEmpty(jsonPartialDataModel))
                {
                    // 3 visibleQuerySourcesTree at Source
                    dynamic dDataModel = JObject.Parse(jsonPartialDataModel);
                    var visibleQuerySources = JsonConvert.DeserializeObject<List<DataSourceItem>>(Convert.ToString(dDataModel.data));

                    // Each Tenant connection need to check but don't have datamodel inside this
                    foreach (var connTenant in dataConnTenant)
                    {
                        // Find connection Datasource from Source Role (DataSource Model)
                        FindConnectionDataSource(sysRole, izTenant, token, tenantRoleId, allpermissionDetail, visibleQuerySources, connTenant);
                    }
                }
            }
        }

        /// <summary>
        /// Fetch Connection data source
        /// </summary>
        /// <param name="sysRole"></param>
        /// <param name="tenant"></param>
        /// <param name="token"></param>
        /// <param name="tenantRoleId"></param>
        /// <param name="allpermissionDetail"></param>
        /// <param name="visibleQuerySources"></param>
        /// <param name="connTenant"></param>
        private static void FindConnectionDataSource(IzRole sysRole, IzTenant tenant, string token, string tenantRoleId, Permission allpermissionDetail, List<DataSourceItem> visibleQuerySources, dynamic connTenant)
        {
            foreach (var querySource in visibleQuerySources)
            {
                string xConnName = querySource.Name.Substring(querySource.Name.IndexOf(']') + 1).Trim().ToUpper();

                // Match DataSource Source and Tenant
                if (xConnName == Convert.ToString(connTenant.name).Trim().ToUpper())
                {
                    // Temp stored for validation, connection and tenant detail
                    var match = _tempMatchList2.FirstOrDefault(tt => tt.Contains(Convert.ToString(tenant.Id.ToString() + querySource.Name).ToUpper()));
                    if (match != null)
                        continue;

                    Console.WriteLine("Source Role with Tenant is matched! Process Started for Role Update, " + querySource.Name + " Tenant:" + tenant.Name);
                    _log.Info("Source Role with Tenant is matched! Process Started for Role Update, " + querySource.Name + " Tenant:" + tenant.Name);

                    List<DataSourceItem> newChild = null;

                    // Fetch Tenant loadPartialDataModelAccess
                    var strDataModelTenant = "{'tenantId':'" + tenant.Id.ToString() + "','roleId':null,'skipItems':0,'pageSize':100000,'parentIds':[],'visibleQuerySourcesTree':[],'criteria':[{'key':'DataSourceName','value':''},{'key':'ShowCheckedQuerySource','value':false}]}";
                    var jsonPartialDataModelTenant = IzendaService.IzendaApi_RequestBody(_hostname + IzEndPointString.LOAD_PARTIAL_DATAMODEL_ACCESS, token, strDataModelTenant);

                    if (!string.IsNullOrEmpty(jsonPartialDataModelTenant))
                    {
                        dynamic dDataModelTenant = JObject.Parse(jsonPartialDataModelTenant);
                        var oTenantDataModel = JsonConvert.DeserializeObject<List<DataSourceItem>>(Convert.ToString(dDataModelTenant.data));

                        foreach (var conCheck in oTenantDataModel)
                        {
                            if (Convert.ToString(querySource.Name).Trim().ToUpper() == Convert.ToString(conCheck.Name).Trim().ToUpper())
                            {
                                // For Validation maintain a List
                                _tempMatchList2.Add(Convert.ToString(tenant.Id.ToString() + querySource.Name).ToUpper());

                                var oTenantDataModel1 = new List<DataSourceItem>();
                                oTenantDataModel1.Add(conCheck);

                                var oSourceDataModel = new List<DataSourceItem>();
                                oSourceDataModel.Add(querySource);

                                newChild = CheckInDataSourceItems(oSourceDataModel, oTenantDataModel1);

                                IzRoleDetail svRole;
                                if (tenantRoleId == null)
                                {
                                    // Role does not exist, creating new
                                    // User + Permissions + visibleQuerySourcesTree
                                    svRole = new IzRoleDetail()
                                    {
                                        Users = null,
                                        Permission = allpermissionDetail,
                                        VisibleQuerySources = null,
                                        Name = sysRole.Name,
                                        TenantId = tenant.Id.ToString(),
                                        Active = true,
                                        IsDirty = true,

                                        Deleted = false,
                                        State = 3,
                                        CreatedBy = "Automation Script",
                                        ModifiedBy = "Automation Script",

                                        Id = tenantRoleId,
                                        VisibleQuerySourcesTree = newChild,
                                        ShowCheckedQuerySource = false,

                                        Inserted = true,
                                        Version = 2,
                                        Created = DateTime.Now,
                                        Modified = DateTime.Now
                                    };
                                }
                                else
                                {
                                    // Role exists, so pull current values and update
                                    var tenantJsonRole = IzendaService.IzendaApi_Request(_hostname + IzEndPointString.ROLE + tenantRoleId, token);
                                    svRole = JsonConvert.DeserializeObject<IzRoleDetail>(tenantJsonRole);
                                    svRole.Permission = allpermissionDetail;
                                    svRole.VisibleQuerySourcesTree = newChild;
                                }

                                var jsonRoleSchema = JsonConvert.SerializeObject(svRole);
                                var sResultRole = IzendaService.IzendaApi_RequestBody(_hostname + IzEndPointString.ROLE, token, jsonRoleSchema);
                                dynamic dataRoleResult = JObject.Parse(sResultRole);

                                if (dataRoleResult.success.ToObject<bool>())
                                {
                                    _log.Info("Output: Success, Role " + sysRole.Name + " " + dataRoleResult.messages + " Tenant: " + tenant.Name);
                                    Console.WriteLine("Output: Success, Role " + sysRole.Name + " " + dataRoleResult.messages + " Tenant: " + tenant.Name);
                                }
                                else
                                {
                                    _log.Info("Output: Failure " + dataRoleResult.messages + " Tenant: " + tenant.Name);
                                    Console.WriteLine("Output: Failure " + dataRoleResult.messages + " Tenant: " + tenant.Name);
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Check datasource item(s)
        /// Recursive call made to set visibility
        /// </summary>
        /// <param name="sourceData"></param>
        /// <param name="destData"></param>
        /// <returns></returns>
        private static List<DataSourceItem> CheckInDataSourceItems(List<DataSourceItem> sourceData, List<DataSourceItem> destData)
        {
            // base case
            if (sourceData.FirstOrDefault().IsLeafItem)
            {
                foreach (var item in sourceData)
                {
                    var tenantItem = destData.FirstOrDefault(x => x.Name.Trim().ToUpper() == item.Name.Trim().ToUpper());

                    if (item.Checked)
                        SetDataSourceItemVisiblity(tenantItem, visible: true);
                    else
                        SetDataSourceItemVisiblity(tenantItem, visible: false);

                    if (tenantItem != null)
                    {
                        if (tenantItem.ChildNodes.Any())
                            CheckInDataSourceItems(item.ChildNodes, tenantItem.ChildNodes);
                        else
                            continue;
                    }
                }
                return destData;
            }

            foreach (var item in sourceData)
            {
                var tenantItem = destData.FirstOrDefault(x => x.Name.Trim().ToUpper() == item.Name.Trim().ToUpper());

                if (item.Checked)
                    SetDataSourceItemVisiblity(tenantItem, visible: true);
                else
                    SetDataSourceItemVisiblity(tenantItem, visible: false);

                if (tenantItem != null && item.ChildNodes.Any())
                    CheckInDataSourceItems(item.ChildNodes, tenantItem.ChildNodes);
                else
                    continue;
            }

            return destData;
        }

        /// <summary>
        /// Set data source item visibility.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="visible"></param>
        /// <returns></returns>
        private static DataSourceItem SetDataSourceItemVisiblity(DataSourceItem item, bool visible)
        {
            if (item != null)
            {
                if (visible)
                {
                    item.Checked = true;
                    item.Interacted = true;
                    item.NumOfCheckedChilds = item.NumOfChilds;
                }
                else
                {
                    item.Checked = false;
                    item.Interacted = true;
                    item.NumOfCheckedChilds = 0;
                }
            }

            return item;
        }

        /// <summary>
        /// Clear our temp list
        /// </summary>
        private static void ResetTempLists()
        {
            _tempMatchList1.Clear();
            _tempMatchList2.Clear();
        }
        #endregion
    }
}
