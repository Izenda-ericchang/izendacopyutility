﻿using CopyManagementUtility.IzendaBoundary.IzendaApiService;
using CopyManagementUtility.IzendaBoundary.IzendaModel;
using Izenda.BI.Framework.Models;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CopyManagementUtility.IzendaBoundary
{
    public class CopySchema
    {
        #region Variables
        private static ILog _log = LogManager.GetLogger(typeof(App));
        private static string _hostname = string.Empty;
        private static bool _completed = false;
        private static List<string> _tempMatchList = new List<string>();
        #endregion

        #region Methods
        /// <summary>
        /// Receive all params and set local variables
        /// </summary>
        /// <param name="hostname"></param>
        /// <param name="token"></param>
        /// <param name="tenantList"></param>
        /// <param name="schemaList"></param>
        /// <param name="copySource"></param>
        /// <returns></returns>
        public static bool ProcessCopySchema(string hostname, string token, IEnumerable<string> tenantList, IEnumerable<string> schemaList, string copySource)
        {
            _tempMatchList.Clear();
            _hostname = hostname;
            _completed = true;

            PrepareSchemaCopy(token, tenantList, schemaList, copySource);

            return _completed;
        }

        /// <summary>
        /// Pre-process before copy schema
        /// </summary>
        /// <param name="token"></param>
        /// <param name="tenantList"></param>
        /// <param name="schemaList"></param>
        /// <param name="copySource"></param>
        private static void PrepareSchemaCopy(string token, IEnumerable<string> tenantList, IEnumerable<string> schemaList, string copySource)
        {
            try
            {
                var response = IzendaService.IzendaApi_Request(_hostname + IzEndPointString.GET_ALLTENANTS, token);
                var tenantListFromServer = JsonConvert.DeserializeObject<List<Tenants>>(response);
                var source = tenantListFromServer.FirstOrDefault(t => t.TenantID == copySource);

                // TODO: Improve this -> use multi-thread approach.
                foreach (var currentTenant in tenantList)
                {
                    ProcessCopy(token, schemaList, tenantListFromServer, currentTenant, source?.Id);
                }
            }
            catch (Exception ex)
            {
                _completed = false;

                Console.WriteLine("Can't process your request. Error:  " + ex.Message);
                _log.Info("Can't process your request. Error:  " + ex.Message);
            }
        }

        /// <summary>
        /// Conduct copy schema
        /// </summary>
        /// <param name="token"></param>
        /// <param name="connectionList"></param>
        /// <param name="tenantListFromServer"></param>
        /// <param name="currentTenant"></param>
        /// <param name="sourceId"></param>
        private static void ProcessCopy(string token, IEnumerable<string> connectionList, List<Tenants> tenantListFromServer, string currentTenant, Guid? sourceId)
        {
            foreach (var tenantInServer in tenantListFromServer)
            {
                // match tenant
                if (tenantInServer.TenantID.Trim().ToUpper() == currentTenant.Trim().ToUpper())
                {
                    try
                    {
                        bool iiCheck = false;
                        foreach (var connection in connectionList)
                        {
                            var jsonConnectionDetail = IzendaService.IzendaApi_Request(_hostname + IzEndPointString.CONNECTION + "/" + tenantInServer.Id.ToString(), token);
                            dynamic dataConnTenant = JArray.Parse(jsonConnectionDetail);

                            Connection targetConnection;

                            if (sourceId == null) // use system level
                            { 
                                // Get system connection with visible data sources
                                var jsonSystemConnections = IzendaService.IzendaApi_Request(_hostname + IzEndPointString.CONNECTION + "/", token);
                                var systemConnections = JsonConvert.DeserializeObject<List<Connection>>(jsonSystemConnections);
                                targetConnection = systemConnections.FirstOrDefault(conn => conn.Name.Equals(connection, StringComparison.InvariantCultureIgnoreCase));
                            }
                            else // use user input source
                            {
                                var connDetailInTenant = IzendaService.IzendaApi_Request(_hostname + IzEndPointString.CONNECTION + "/" + sourceId.ToString(), token);
                                var tentantConnections = JsonConvert.DeserializeObject<List<Connection>>(connDetailInTenant);
                                targetConnection = tentantConnections.FirstOrDefault(conn => conn.Name.Equals(connection, StringComparison.InvariantCultureIgnoreCase));
                            }

                            if (dataConnTenant.Count > 0 && targetConnection != null)
                            {
                                foreach (var connTenant in dataConnTenant)
                                {
                                    var dbNameInServer = Convert.ToString(connTenant.name).Trim();
                                    var isMatched = dbNameInServer.Equals(connection, StringComparison.InvariantCultureIgnoreCase);

                                    // match connection string
                                    if (isMatched)
                                    {
                                        // Temp stored for validation, connection and tenant detail
                                        iiCheck = true;

                                        var match = _tempMatchList.FirstOrDefault(tt => tt.Contains(Convert.ToString(currentTenant + connection).ToUpper()));
                                        if (match != null)
                                            continue;

                                        Console.WriteLine("\nMatched: " + currentTenant + " " + connection);
                                        
                                        // Reload Schema or updated Datasource information
                                        var postReloadSchema = "{'connectionId':'" + connTenant.id + "','connectionString':'" + connTenant.connectionString + "','serverTypeId':'" + connTenant.serverTypeId + "'}";

                                        if (!string.IsNullOrEmpty(postReloadSchema))
                                        {
                                            var jsonReloadSchema = IzendaService.IzendaApi_RequestBody(_hostname + IzEndPointString.RELOAD_REMOTE_SCHEMA, token, postReloadSchema);
                                            dynamic dataReloadSchema = JObject.Parse(jsonReloadSchema);

                                            if(dataReloadSchema.success.ToObject<bool>())
                                            {
                                                //Get tenant Connection details
                                                var sResultConnTenant = IzendaService.IzendaApi_Request(_hostname + IzEndPointString.GET_CONNECTION_DETAIL + "/" + connTenant.id.ToString(), token);
                                                dynamic dataConnection = JObject.Parse(sResultConnTenant);

                                                if (dataConnection.success.ToObject<bool>())
                                                {
                                                    _log.Info("Process Start for Updating Schema: " + connTenant.name + " for Tenant:" + tenantInServer.TenantID);
                                                    Console.WriteLine("Process Start for Updating Schema: " + connTenant.name + " for Tenant:" + tenantInServer.TenantID);
                                                    Connection allSchemaDetail = JsonConvert.DeserializeObject<Connection>(Convert.ToString(dataConnection.connection));

                                                    if (allSchemaDetail.Visible)
                                                    {
                                                        //For Vaidation maintain a List
                                                        _tempMatchList.Add(Convert.ToString(currentTenant + connection).ToUpper());

                                                        SaveUpdatedConnection(targetConnection, allSchemaDetail, token);
                                                    }
                                                    else
                                                    {
                                                        Console.WriteLine("This connection is currently hidden. You cannot edit anything. Please change this connection to visible then edit again. for Tenant:" + tenantInServer.TenantID + " DataSource:" + connTenant.name);
                                                        _log.Info("This connection is currently hidden. You cannot edit anything. Please change this connection to visible then edit again. for Tenant:" + tenantInServer.TenantID + " DataSource:" + connTenant.name);
                                                    }
                                                }
                                                else
                                                {
                                                    _log.Info("Connection Not Found, for " + tenantInServer.TenantID + " " + connTenant.name);
                                                    Console.WriteLine("Connection Not Found, for " + tenantInServer.TenantID + " " + connTenant.name);
                                                }
                                            }
                                            else
                                            {
                                                _log.Info("Connection Not Found, for " + tenantInServer.TenantID + " " + connTenant.name);
                                                Console.WriteLine("Connection Not Found, for " + tenantInServer.TenantID + " " + connTenant.name);
                                            }
                                        }
                                    }

                                    _tempMatchList.Clear();
                                }
                            }
                            else
                            {
                                _log.Info("Tenant doesn't have any Connection, Tenant:" + tenantInServer.TenantID);
                                Console.WriteLine("\nTenant doesn't have any Connection, Tenant:" + tenantInServer.TenantID);
                            }
                        }

                        if (iiCheck)
                        {
                            _log.Info("Connection not matched, Tenant:" + tenantInServer.TenantID);
                            Console.WriteLine("\nConnection not matched, Tenant:" + tenantInServer.TenantID);
                        }
                        else
                        {
                            _log.Info("No connections have been copied. Inconsistency between origin and target, Tenant:" + tenantInServer.TenantID);
                            Console.WriteLine("\nNo connections have been copied. Inconsistency between origin and target, Tenant:" + tenantInServer.TenantID);
                        }
                    }
                    catch (Exception ex)
                    {
                        _completed = false;

                        Console.WriteLine(tenantInServer.TenantID + ", Tenant has error while processing. Error:  " + ex.Message);
                        _log.Info(tenantInServer.TenantID + ", Tenant has error while processing. Error:  " + ex.Message);
                    }
                }
            }
        }

        /// <summary>
        /// Save updated connection.
        /// </summary>
        /// <param name="sourceConnection"></param>
        /// <param name="tenantConnection"></param>
        /// <param name="token"></param>
        private static void SaveUpdatedConnection(Connection sourceConnection, Connection tenantConnection, string token)
        {
            // Get data source details of origin
            var sourceConnResponse = IzendaService.IzendaApi_Request(_hostname + IzEndPointString.GET_CONNECTION_DETAIL + "/" + sourceConnection.Id.ToString(), token);
            var connDetails = JsonConvert.DeserializeObject<IzConnectionResult>(sourceConnResponse).Connection;

            // Clear visible data sources in tenant Connection
            ClearVisibleDataSources(tenantConnection);

            // Match the query source visibility in each schema from origin to tenant
            var sourceQuerySources = connDetails.DBSource.QuerySources;
            var tenantQuerySources = tenantConnection.DBSource.QuerySources;

            foreach (var schema in sourceQuerySources)
            {
                var tenantSchema = tenantQuerySources.FirstOrDefault(q => q.Name == schema.Name);
                if (tenantSchema != null)
                {
                    var visibleQuerySources = schema.QuerySources.Where(q => q.Selected).ToList();
                    foreach (var querySource in visibleQuerySources)
                    {
                        // find corresponding tenant query source by name
                        var tenantQuerySource = tenantSchema.QuerySources.FirstOrDefault(qs => qs.Name == querySource.Name);

                        if (tenantQuerySource != null)
                            tenantQuerySource.Selected = true;
                    }
                }
            }

            // Save Connection after query sources are updated
            var tenantConnectionJson = JsonConvert.SerializeObject(tenantConnection);
            var result = IzendaService.IzendaApi_RequestBody(_hostname + IzEndPointString.CONNECTION, token, tenantConnectionJson);
            var dataResult = JsonConvert.DeserializeObject<SaveConnectionStatus>(result);

            if (dataResult.Success)
            {
                _log.Info("Output: Success " + result + " " + dataResult.Messages + " " + tenantConnection.Name);
                Console.WriteLine("Output: Success " + result + " " + dataResult.Messages + " " + tenantConnection.Name);
            }
            else
            {
                _log.Info("Output: Failure " + dataResult.Messages + " " + tenantConnection.Name);
                Console.WriteLine("Output: Failure " + dataResult.Messages + " " + tenantConnection.Name);
            }
        }

        /// <summary>
        /// Clear out all visible data queary sources
        /// </summary>
        /// <param name="connection"></param>
        private static void ClearVisibleDataSources(Connection connection)
        {
            foreach (var querySource in connection.DBSource.QuerySources.SelectMany(schema => schema.QuerySources))
            {
                querySource.Selected = false;
            }
        } 
        #endregion
    }
}
