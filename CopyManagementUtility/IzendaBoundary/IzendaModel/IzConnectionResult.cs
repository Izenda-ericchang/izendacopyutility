﻿using Izenda.BI.Framework.Models;
using System.Collections.Generic;

namespace CopyManagementUtility.IzendaBoundary.IzendaModel
{
    public class IzConnectionResult
    {
        #region Properties
        public Connection Connection { get; set; }

        public bool Success { get; set; }

        public List<ModelError> Messages { get; set; }

        public object Data { get; set; }
        #endregion
    }
}
