﻿using Izenda.BI.Framework.Models;
using Izenda.BI.Framework.Models.Permissions;
using System;
using System.Collections.Generic;

namespace CopyManagementUtility.IzendaBoundary.IzendaModel
{
    public class IzRoleDetail
    {
        #region Properties
        public List<IzTenant> Users { get; set; }

        public Permission Permission { get; set; }

        public List<VisibleQuerySource> VisibleQuerySources { get; set; }

        public string Name { get; set; }

        public string TenantId { get; set; }

        public bool Active { get; set; }

        public bool Deleted { get; set; }

        public int State { get; set; }

        public bool Inserted { get; set; }

        public int Version { get; set; }

        public DateTime Created { get; set; }

        public string CreatedBy { get; set; }

        public DateTime Modified { get; set; }

        public string ModifiedBy { get; set; }

        public bool IsDirty { get; set; }

        public string Id { get; set; }

        public List<DataSourceItem> VisibleQuerySourcesTree { get; set; }

        public bool ShowCheckedQuerySource { get; set; }
        #endregion
    }

    public class VisibleQuerySource
    {
        #region Properties
        public string Id { get; set; }

        public List<QuerySourceField> QuerySourceFields { get; set; }
        #endregion
    }
}
