﻿using System;

namespace CopyManagementUtility.IzendaBoundary.IzendaModel
{
    public class IzRole
    {
        #region Properties
        public Guid RoleId { get; set; }

        public string Name { get; set; }
        #endregion
    }
}
