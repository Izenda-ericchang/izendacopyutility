﻿using System;

namespace CopyManagementUtility.IzendaBoundary.IzendaModel
{
    public class IzUserRoleDetail
    {
        #region Properties
        public Guid Id { get; set; }

        public string Name { get; set; }
        #endregion
    }
}
