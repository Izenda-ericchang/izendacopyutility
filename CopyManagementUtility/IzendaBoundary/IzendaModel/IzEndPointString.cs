﻿namespace CopyManagementUtility.IzendaBoundary.IzendaModel
{
    public static class IzEndPointString
    {
        #region CONSTS
        // TENANT
        public const string GET_ALLTENANTS = "/api/tenant/allTenants";
        public const string TENANT = "/api/tenant/";

        // DB
        public const string GET_SUPPORTED_DBTYPE = "/api/databaseSetup/SupportedDatabaseType";
        public const string POST_DB_INFO = "/api/databaseSetup/DatabaseInfo";

        // CONNECTION
        public const string CONNECTION = "/api/connection";
        public const string TESTDB = "/api/connection/DatabaseName";
        public const string GET_CONNECTION_DETAIL = "/api/connection/detail";
        public const string GET_CONNECTION_DETAIL_INFO = "/api/connection/detailInfo/";
        public const string VERIFY_CONNECTION_DB = "/api/connection/verify";
        public const string VISIBLE_CONNECTION = "/api/connection/visible/count";
        public const string LOAD_REMOTE_SCHEMA = "/api/connection/loadRemoteSchema";
        public const string RELOAD_REMOTE_SCHEMA = "/api/connection/reloadRemoteSchema";

        // ROLE
        public const string ROLE = "/api/role/";
        public const string GET_ALL_ROLES = "/api/role/all/";
        public const string GET_ALL_ROLES_OPTIONS = "/api/role/all/?includeHashcode=false&basicInfo=false";
        public const string ALL_CATEGORIES = "/api/role/availableCategory/";
        public const string LOAD_PARTIAL_DATAMODEL_ACCESS = "/api/role/loadPartialDataModelAccess";
        public const string LOAD_ACCESSRIGHT = "/api/accessright/load";

        // USER
        public const string GET_ALLUSERS = "/api/user/all/"; 
        #endregion
    }
}
