﻿using CopyManagementUtility.ViewModel;
using log4net;
using System;
using System.IO;
using System.Net;
using System.Text;

namespace CopyManagementUtility.IzendaBoundary.IzendaApiService
{
    public static class IzendaService
    {
        #region Variables
        private static readonly ILog _log = LogManager.GetLogger(typeof(CopyManagementVM));
        private static string _userName = string.Empty;
        private static string _password = string.Empty;
        #endregion

        #region Methods
        public static string IzendaApi_Request(string url, string token)
        {
            try
            {
                var req = WebRequest.Create(url);
                req.Method = "GET";
                req.Headers["Authorization"] = "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes("username:password"));
                req.Headers["access_token"] = token;
                req.Credentials = new NetworkCredential(_userName, _password);

                var response = req.GetResponse();
                string responseFromServer;

                using (Stream dataStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(dataStream);
                    responseFromServer = reader.ReadToEnd();
                }
                response.Close();

                return responseFromServer;
            }
            catch (Exception ex)
            {
                _log.Error("----------------\n" + ex.ToString());
                Console.WriteLine("----------------\n" + ex.ToString());

                return ex.ToString();
            }
        }

        public static string IzendaApi_RequestBody(string url, string token, string postData)
        {
            try
            {
                var request = WebRequest.Create(url);
                request.Method = "POST";
                request.Headers["access_token"] = token;
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                request.ContentType = "application/json";
                request.ContentLength = byteArray.Length;
                var dataStream = request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();

                var response = request.GetResponse();
                var responseFromServer = string.Empty;

                using (dataStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(dataStream);
                    responseFromServer = reader.ReadToEnd();
                }
                response.Close();

                return responseFromServer.ToString();
            }
            catch (Exception ex)
            {
                _log.Error("----------------\n" + ex.ToString());
                Console.WriteLine("----------------\n" + ex.ToString());

                return ex.ToString();
            }
        } 
        #endregion
    }
}
