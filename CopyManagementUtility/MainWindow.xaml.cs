﻿using CopyManagementUtility.ViewModel;
using System.Windows;

namespace CopyManagementUtility
{
    public partial class MainWindow : Window
    {
        #region CTOR
        public MainWindow()
        {
            InitializeComponent();

            DataContext = new CopyManagementVM();
        } 
        #endregion
    }
}
